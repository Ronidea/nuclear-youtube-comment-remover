# Nuclear Youtube Comment Remover

Delete all the Youtube comments you wrote

YouTube doesn't offer you a tool to delete all the comments you've made by one click.  
Neither does this tool. But at least it does the clicking for you.  



### INSTRUCTIONS:

    NOTE: The tool uses Chromium/Chrome browser by default.
    Therefor the Chromium/Chrome browser needs to be installed.
    You can adjust the script to use different browsers, but you'll probably have to debug it.  

##### Prerequisites
 
1. Python3  https://www.python.org/downloads/  
2. Chrome 	https://www.google.com/chrome/  


##### Install and Run

1. Download: https://gitlab.com/Ronidea/nuclear-youtube-comment-remover/-/archive/master/nuclear-youtube-comment-remover-master.zip  
2. Extract the zip-file
3. Open the folder you just extracted ("nuclear-youtube-comment-remover-master")
4. Double-click on "Nuclear-Youtube-Comment-Remover(.py)"

### FAQ:

Q: Why do you rather automize all the clicking than simply directly sending POST requests to the YT-server?  
A: Because I'm a lazy person and that's all I needed.

Q: Why don't you  implement proper error handling?  
A: Same as above.

Q: Why does Nuclear YouTube Comment Remover only runs on Linux?!  
A: It now should run on Windows as well (tested on win10)

Q: Is it save to enter my login credentials into your program?  
A: Yes. But you should ALWAYS study the source-code before doing that.  
   The code is easy to read and extensively commented.

Q: Is your tool good?  
A: Not really, but it does it's job (somewhat).

Q: Can I donate you money for the tool?  
A: No.

Q: Were you ever asked one of these questions?  
A: No.