#!/usr/bin/python3

#   IMPORTS

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import codecs
import time
import json
import sys
import os


#   VARIABLE DECLARATION

global comment_history
global email_or_phone
global password
global driver
global speed
global run
global print_delay


#   VARIABLE INITIALIZING

comment_history = ""    #   will hold deleted comments
print_delay     = 0.08  #   delay after printing with print_pretty()
run             = True  #


#   METHODS

#   removes all previous content inside the terminal
def clear_terminal():
       if sys.platform.startswith("linux"):
        os.system("clear")  # printf "\033c"
    elif sys.platform.startswith("darwin"):
        os.system("clear")  # && printf '\\e[3J'")     
    elif sys.platform.startswith("win"):
        os.system("cls")

    time.sleep(print_delay)

#   creates a little animation by printing with 'print_delay' delay
def print_pretty(text):
    # print the actual text
    print(text)
    # wait for print_delay seconds
    time.sleep(print_delay)

#   prints a welcome message
def print_welcome_text():
    print_pretty("####################################################")
    print_pretty("###  Welcome to Nuclear-Youtube-Comment-Remover  ###")
    print_pretty("###  The tool to delete an emberassing history   ###")
    print_pretty("####################################################")
    print("")
    print("")

#   asks user for youtube login data
def get_youtube_login_data():
    # use variable(s) declared in global scope
    global email_or_phone
    global password
    print_pretty("###  YOUTUBE LOGIN  ###")
    email_or_phone = input("email/phone: ") # get login email/phone from user
    time.sleep(print_delay)                 # pretty print (but with input)
    password = input("password: ")          # get password from user

#   opens the youtube comment history webpage
def open_youtube_comment_history():
    # use variable(s) declared in global scope
    global driver

    if sys.platform.startswith("linux"):
        driver = webdriver.Chrome("./linux/chromedriver")
    elif sys.platform.startswith("darwin"):
        driver = webdriver.Chrome("./mac/chromedriver")
    elif sys.platform.startswith("win"):
        driver = webdriver.Chrome("./windows/chromedriver")

    driver.get("https://www.youtube.com/feed/history/comment_history")  #open comment history webpage

#   sets youtubes language to english
def set_english_language():
    # use variable(s) declared in global scope
    global driver
    time.sleep(0.5)                                                                   # wait for page to load
    driver.find_elements_by_tag_name("ytd-topbar-menu-button-renderer")[2].click()  # open settings
    driver.find_element_by_tag_name("ytd-compact-link-renderer").click()            # open language settings
    time.sleep(0.5)                                                                 # adding sleep here fixed some issues when I ran it on my windows machine... 
    lang_choices = driver.find_elements_by_tag_name("paper-item")                   # get all language choices

    # search for language settings-item with text 'English (US)'
    for item in lang_choices:
        if (item.get_attribute("innerText") == "English (US)"):
            item.click()    # set language to English (US)
            break           #  leave the search

#   logs into youtube with the previously given login data
def login_youtube():
    # use variable(s) declared in global scope
    global driver
    global email_or_phone
    global password
    driver.find_element_by_link_text("Sign in").click()                 # open sign-in page
    driver.find_element_by_name("identifier").send_keys(email_or_phone) # enter email/phone
    driver.find_element_by_id("identifierNext").click()                 # press next
                                                                        # wait until password textfield is loaded and enter password
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "password"))).send_keys(password)
    driver.find_element_by_id("passwordNext").click()                   # press next
    input("\nNOTE: Please select your account, if a dialog is shown. Press Enter to continue ..")

#   prints menu to configure delay between each click
def print_speed_menu():
    # use variable(s) declared in global scope
    global speed
    global choice

    print_pretty("")
    print_pretty("")
    print_pretty("###  SPEED MENU  ###" )
    print_pretty("If you have a slow webbrowser experience, please select a lower speed (1-3).")
    print_pretty("")
    print_pretty("1)  LAZY HAMSTER"     )   # corrensponds to 0.5   seconds delay between each click when deleting comments
    print_pretty("2)  SLEEPY HAMSTER"   )   # corrensponds to 0.25  seconds delay between each click when deleting comments
    print_pretty("3)  NORMAL HAMSTER"   )   # corrensponds to 0.2   seconds delay between each click when deleting comments
    print_pretty("4)  SPEEDY HAMSTER"   )   # corrensponds to 0.1   seconds delay between each click when deleting comments
    print_pretty("5)  ADRENALIN HAMSTER")   # corrensponds to 0.04  seconds delay between each click when deleting comments
    choice = int(input("\nspeed: "))

    if choice == 1:
        speed = 0.5
    elif choice == 2:
        speed = 0.25
    elif choice == 3:
        speed = 0.2
    elif choice == 4:
        speed = 0.1
    elif choice == 5:
        speed = 0.04
    elif choice >= 10:
        speed = choice/1000 # custom choice :)
    else:
        speed = 0.1        # default

#   delets all youtube comments until 10 errors in a row
def delete_comments():
    error_tolerance = 10    # max errors before aborting program
    error_latency   = 1     # pause after each  error
    error_counter   = 0     # counter of errors in a row

    global driver
    global run
    input("WARNING, THIS WILL START DELETING ALL YOUR YOUTUBE'S COMMENT HISTORY!\nPress Enter to continue")
    while (run):
        try:
            comment = driver.find_element_by_tag_name("ytd-comment-history-entry-renderer") # find first comment
            save_comments(comment)                                                          # save comment to list
            comment.find_element_by_id("button").click()                                    # find 3-dot button and click it
            time.sleep(speed)                                                               # wait for animation ..
            driver.find_element_by_link_text("Delete").click()                              # find the drop-down-menu-item "Delete" and click it
            time.sleep(speed)                                                               # wait for animation ..
            driver.find_element_by_link_text("DELETE").click()                              # find the dialog-button "DELETE" and click it
            time.sleep(speed)                                                               # wait for animation ..

            # remove the now invisible item from the page by javascript-command
            driver.execute_script("""var element = arguments[0]; element.parentNode.removeChild(element);""", comment)

            error_counter = 0 # reset error_counter
        except:
            error_counter += 1
            # Some times loading takes a bit. I use use an err_counter + sleep() instead of WebDriverWait here. Why? Read FAQs
            if (error_counter < error_tolerance):
                # display how many errors in a row we have
                print("Error! tolerance: (%d/%d) - trying  to continue .." % (error_counter, error_tolerance))
                # wait for error_latency seconds. Maybe new comments  are loaded now ...
                time.sleep(error_latency)
            else:
                # after error_tolerance is reached, abord the program and save the deleted comments to textfile
                print("Error! tolerance: (%d/%d) - aborting .." % (error_counter, error_tolerance))
                export_comments()
                break

#   adds a comment's text to comment_history
def save_comments(comment):
    # use variable(s) declared in global scope
    global comment_history
    # prettyfying the textfile :)
    comment_history = comment_history + "--------\n" + comment.text + "\n--------\n\n\n"

#   saves comment_history to a text file
def export_comments():
    # skip if comment_history empty
    if (comment_history == ""):
        return

    path = str(Path.home()) + '/comment-history.txt'  # get user path, append file name
    f = codecs.open(path, 'a', encoding='utf-8')        # open file at path, utf8 encoding
    f.write(comment_history)                            # write  commenthistory to textfile
    print("Saved comment history to: " + path)          # print  path of saved file
    input("Press Enter to view all deleted comments: ")
    
    if sys.platform.startswith("linux"):
        os.system("xdg-open" + path)
    elif sys.platform.startswith("darwin"):
        os.system("open" + path)     
    elif sys.platform.startswith("win"):
        os.system("open" + path)


# EXECUTE METHODS

clear_terminal()

print_welcome_text()

get_youtube_login_pw()

print_speed_menu()

open_youtube_comment_history()

set_english_language()

login_youtube()

delete_comments()
